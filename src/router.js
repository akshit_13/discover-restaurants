import Vue from "vue";
import Router from "vue-router";
import Login from "./views/Login.vue";
const RestaurantList = () =>
  import(/* webpackChunkName: "router-RestaurantList" */ "./views/RestaurantList.vue");
const RestaurantDetails = () =>
  import(/* webpackChunkName: "router-RestaurantDetails" */ "./views/RestaurantDetails.vue");

Vue.use(Router);

const router = new Router({
  routes: [
    {
      path: "/",
      name: "Login",
      component: Login,
      meta: {
        requiresGuest: true
      }
    },
    {
      path: "/restaurant",
      name: "RestaurantList",
      component: RestaurantList,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: "/restaurant/:id",
      name: "RestaurantDetails",
      component: RestaurantDetails,
      meta: {
        requiresAuth: true
      }
    }
  ],
  mode: "history"
});

router.beforeEach((to, from, next) => {
  let isLoggedIn = localStorage.getItem("isLoggedIn") == "true";
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!isLoggedIn) {
      next({
        path: "/",
        query: {
          redirect: to.fullPath
        }
      });
    } else {
      next();
    }
  } else if (to.matched.some(record => record.meta.requiresGuest)) {
    if (isLoggedIn) {
      next({
        path: "/restaurant",
        query: {
          redirect: to.fullPath
        }
      });
    } else {
      next();
    }
  } else {
    next();
  }
});

export default router;
